import boto3 ##you will need to install this
from boto3.dynamodb.conditions import Key, Attr
import time

def send_to_postsDB (post_guid,main_folder,sub_folder,title,content,post_url):
    
    session = boto3.Session(aws_access_key_id='AKIA2RIWITYQZF3KIVHY', aws_secret_access_key='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')

    dynamodb = session.resource('dynamodb', region_name='eu-west-2')

    table = dynamodb.Table('Posts') 
    time_stamp = time.strftime('%a, %d %b %Y %H:%M:%S')
    
    response = table.put_item(
       Item={
            'post_id': post_guid,
            'post_title' :title,            
            'post_content' : content,            
            'post_folder': main_folder,
            'post_subdir' : sub_folder,            
            'post_time_stamp' :time_stamp,    
            'post_url' : post_url
        }
    )

def send_to_commentsDB (post_guid,comment_guid,comment_content):
    
    session = boto3.Session(aws_access_key_id='AKIA2RIWITYQZF3KIVHY', aws_secret_access_key='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')

    dynamodb = session.resource('dynamodb', region_name='eu-west-2')

    table = dynamodb.Table('Comments') 

    time_stamp = time.strftime('%a, %d %b %Y %H:%M:%S')
    
    response = table.put_item(
       Item={
            'comment_id':comment_guid,
            'post_id': post_guid,
            'comment_content':comment_content,
            'comment_time_stamp' :time_stamp    

        }
    )    

def send_to_repliesDB (comment_guid,reply_guid,reply_content):
    
    session = boto3.Session(aws_access_key_id='AKIA2RIWITYQZF3KIVHY', aws_secret_access_key='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')

    dynamodb = session.resource('dynamodb', region_name='eu-west-2')

    table = dynamodb.Table('Replies') 

    time_stamp = time.strftime('%a, %d %b %Y %H:%M:%S')
    
    response = table.put_item(
       Item={
            'comment_id':comment_guid,
            'reply_id': reply_guid,
            'reply_content':reply_content,
            'reply_time_stamp' :time_stamp    
        }
    )

def is_scraped (post_url):
    dynamodb_session = boto3.Session(aws_access_key_id='AKIA2RIWITYQZF3KIVHY',
      aws_secret_access_key='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',
      region_name='eu-west-2')

    dynamodb = dynamodb_session.resource('dynamodb')
        
    table = dynamodb.Table('Posts')
    
    response = table.scan(
        FilterExpression=Attr('post_url').eq(post_url)
    )
    

    if response['Items'] == []:
        return False
    else:
        return True
