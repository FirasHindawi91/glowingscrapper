from pyvirtualdisplay  import Display
from selenium import webdriver
from tbselenium.tbdriver import TorBrowserDriver

from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import requests
import os
import bs4 as bs
import csv
import uuid
import time
from ScrapComments_AWS import *
from send_to_DB import *

def Scrap(community_url,main_folder,sub_folder):
    
    StartTime = time.time()
    display = Display(visible=0, size=(1024, 768))
    display.start()

    new_titles_list=[]
    new_contents_list=[]
    csv_row=[]
    counter1 = 0
    
    with TorBrowserDriver('/home/ubuntu/newrepo/glowingscrapper/tor-browser_en-US/') as driver:
   
        for page in range (0,3):
            driver.set_page_load_timeout(60)
            try:
                driver.load_url(url = community_url + str(page), wait_on_page=60, wait_for_page_body=True)
            except:
                driver.refresh()
                driver.load_url(url = community_url + str(page), wait_on_page=60, wait_for_page_body=True)
                
##            driver.load_url(url = community_url + str(page), wait_on_page=60, wait_for_page_body=True)
##            driver.get(community_url + str(page))
            soup = bs.BeautifulSoup(driver.page_source, 'lxml')
            topicBody = soup.find_all('a', attrs={"id":"topic-item"})
                    
            for item in topicBody:
                counter1+=1
                post_url = "https://glowing.com"+ item.get('href')

                if is_scraped(post_url):
                    print ("This Post '"+post_url + "' already exists")
                else:
                    
                    post_guid = str(uuid.uuid1())
                    
                    try:
                        title = (item.find('p',class_='title').text)
                    except:
                        title = ("non textual")

                    try:
                        content = (item.find('p',class_='content').text)
                    except:
                        content = ("non textual")

                    if title == "":
                        title = "empty text"
                    if content == "":
                        content = "empty text"
                        
                    print(main_folder+"/"+sub_folder+"/ Page #"+str(page) +" post #"+str(counter1))
                    print(post_url)
                    
                    ScrapComments(post_guid,post_url,driver)
##                    try:
##                        ScrapComments(post_guid,post_url,driver)
##                    except:
##                        driver.refresh()
##                        ScrapComments(post_guid,post_url,driver)
##                        print ("was not able to Scrap Comments")
                    
                    send_to_postsDB (post_guid,main_folder,sub_folder,title,content,post_url)
            

            print ("\n\n                                      ------------Page #"+str(page)+ " is finished-------------")


        EndTime = (time.time() - StartTime)/60
        print("Scraping "+ main_folder + "/"+ sub_folder + " is complete")
        print("Time needed = " + str(EndTime) + " Minutes")
        driver.close()
    display.stop()
    
Scrap("https://glowing.com/community/group/1?page=","Getting Pregnant","general TTC")
Scrap("https://glowing.com/community/group/621?page=","Getting Pregnant","Secondary Infertility TTC")
Scrap("https://glowing.com/community/group/75?page=","Getting Pregnant","TTC with PCOS")



##def Scrap(url,folder_path,csv_name):
##    
##    StartTime = time.time()
##    
##    os.makedirs(folder_path+'./Posts/', exist_ok = True)
##    
##
##    display = Display(visible=0, size=(1024, 768))
##    display.start()
##
##               
##    siteName = url
##
##    new_titles_list=[]
##    new_contents_list=[]
##    csv_row=[]
##    counter1 = 0
##    
##    with TorBrowserDriver('/home/ubuntu/newrepo/glowingscrapper/tor-browser_en-US/') as driver:
##   
##        for page in range (0,100):
##
####            driver.implicitly_wait(150)
##            driver.get(siteName + str(page))
##            soup = bs.BeautifulSoup(driver.page_source, 'lxml')
##            topicBody = soup.find_all('a', attrs={"id":"topic-item"})
##                    
##            for item in topicBody:
##                
##                guid = str(uuid.uuid1())
##                #Creating the Posts folder inside the folder path
##                PostsPath = folder_path+'./Posts/'+guid
##                os.makedirs(PostsPath, exist_ok = True)
##                
##                #Creating the Comments folder inside the Posts folder
##                CommentsPath = folder_path+'./Posts/'+guid+'./Comments/'
##                os.makedirs(CommentsPath, exist_ok = True)
##                
##                Postlink = "https://glowing.com"+ item.get('href')
##     
##                try:
##                    new_titles_list.append(item.find('p',class_='title').text)
##                except :
##                    new_titles_list.append("Missing")
##
##                try:
##                    new_contents_list.append(item.find('p',class_='content').text)
##                except :
##                    new_contents_list.append("Missing")
##
##                ScrapComments(Postlink,CommentsPath,driver)
##                
##                send_to_postsDB (guid,main_folder,sub_folder,title,content,url)
##    ##            print(csv_row)
##                csv_row.append([new_titles_list[counter1],guid,Postlink])
##                with open (PostsPath+'/'+guid+'.txt', 'w' ,newline='',encoding="utf-8") as contents:
##                    contents.write(new_contents_list[counter1])
##                counter1 +=1
##            
##
##            print ("\n\n                                      ------------Page #"+str(page)+ " is finished-------------")
##
##        with open (folder_path+csv_name, 'w' , newline='',encoding="utf-8") as resultFile:
##            wr = csv.writer(resultFile, dialect='excel')
##            wr.writerows(csv_row)
##
##        EndTime = (time.time() - StartTime)/60
##        print("Scraping "+ folder_path + " is complete")
##        print("Time needed = " + str(EndTime) + " Minutes")
##        driver.close()
##    display.stop()
##
### Getting Pregnant
##Scrap("https://glowing.com/community/group/1?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/general TTC/","General TTC.csv")
##Scrap("https://glowing.com/community/group/621?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/Secondary Infertility TTC/","Secondary Infertility TTC.csv")
####Scrap("https://glowing.com/community/group/75?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/TTC with PCOS/","TTC with PCOS.csv")
####Scrap("https://glowing.com/community/group/72057594037928021?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/TTC Newbies/","TTC Newbies.csv")
####Scrap("https://glowing.com/community/group/53?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/Faint LinesNO BFPs/","Faint LinesNO BFPs.csv")
####Scrap("https://glowing.com/community/group/49?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/Assisted Fertility/","Assisted Fertility.csv")
####Scrap("https://glowing.com/community/group/209?page=","/home/ubuntu/newrepo/glowingscrapper/Getting Pregnant/OPK & Ovulation Help/","OPK & Ovulation Help.csv")
####
#####Fertility Issues & Concerns
####Scrap("https://glowing.com/community/group/51?page=","/home/ubuntu/newrepo/glowingscrapper/Fertility Issues & Concerns/BBT Help & Chart Analysis/","BBT Help & Chart Analysis.csv")
####Scrap("https://glowing.com/community/group/72057594037928043?page=","/home/ubuntu/newrepo/glowingscrapper/Fertility Issues & Concerns/TTC with Endometriosis/","TTC with Endometriosis.csv")
####Scrap("https://glowing.com/community/group/73?page=","/home/ubuntu/newrepo/glowingscrapper/Fertility Issues & Concerns/TTC with Male-Factor Infertility/","TTC with Male-Factor Infertility.csv")
####
#####Pregnancy
####Scrap("https://glowing.com/community/group/72057594037928153?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Breastfeeding/","Breastfeeding.csv")
####Scrap("https://glowing.com/community/group/72057594037928009?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/First Time Moms/","First Time Moms.csv")
####Scrap("https://glowing.com/community/group/72057594037927937?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/General Pregnancy/","General Pregnancy.csv")
####Scrap("https://glowing.com/community/group/72057594037928775?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Gestational Diabetes/","Gestational Diabetes.csv")
####Scrap("https://glowing.com/community/group/72057594037928483?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Postpartum Mental Health Support/","Postpartum Mental Health Support.csv")
####Scrap("https://glowing.com/community/group/283?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Pregnancy & Exercise/","Pregnancy & Exercise.csv")
####Scrap("https://glowing.com/community/group/72057594037928141?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Pregnant with PCOS/","Pregnant with PCOS.csv")
####Scrap("https://glowing.com/community/group/72057594037928063?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Pregnancy Health Concerns/","Pregnancy Health Concerns.csv")
####Scrap("https://glowing.com/community/group/72057594037928023?page=","/home/ubuntu/newrepo/glowingscrapper/Pregnancy/Product Recommendations/","Product Recommendations.csv")
####
#####Community Groups
####Scrap("https://glowing.com/community/group/72057594037928153?page=","/home/ubuntu/newrepo/glowingscrapper/Community Groups/General Pregnancy/","General Pregnancy.csv")
####Scrap("https://glowing.com/community/group/72057594037928009?page=","/home/ubuntu/newrepo/glowingscrapper/Community Groups/Polycystic Ovarian Syndrome/","Polycystic Ovarian Syndrome.csv")

