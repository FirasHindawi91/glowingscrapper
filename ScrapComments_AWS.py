from pyvirtualdisplay  import Display
from selenium import webdriver
from tbselenium.tbdriver import TorBrowserDriver
import requests
import os
import bs4 as bs
import csv
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
import uuid
import time
from send_to_DB import *

def click_load_button(self):
        """ click the button that redirects to the list of all webcams

            Raises:
                UnexpectedAlertPresentException: it occurs when a ramdom alert dialog pops up
                TimeoutException: it occurs when the element we want is never loaded
        """
        try:
            # wait until the element is loaded
            self.wait.until(lambda driver: driver.find_element_by_id("down-arrow"))

            # get the button element and click it
            button = self.driver.find_element_by_id("down-arrow")
            button.click()
##
        except UnexpectedAlertPresentException:
            self.handle_alert()
            self.click_load_button()

        except:
            self.driver.refresh()
            self.click_load_button()

def ScrapComments(post_guid,post_url,driver):
    print ("Entering ScrapComments")
##    driver.load_url(url = post_url, wait_on_page=0, wait_for_page_body=True)
##    try:
##        driver.load_url(url = post_url, wait_on_page=60, wait_for_page_body=True)
##    except:
##        driver.refresh()
##        driver.load_url(url = post_url, wait_on_page=60, wait_for_page_body=True)
    try:
        driver.load_url(url = post_url, wait_on_page=0, wait_for_page_body=True)
        
        soup = bs.BeautifulSoup(driver.page_source, 'lxml')
        
        No_Replies_Flag = len(soup.find_all('p',class_='no-replies'))
        Down_Arrow_Flag = len(soup.find_all('div',class_='load-more'))
        Num_Comments = len(soup.find_all('div',attrs={"id":"topic-reply"}))    

        CommentsList = []
        RepliesList = []
        
        CommentsCounter = 0
        RepliesCounter = 0

    ##    print ("Entering if Statement")        
        if No_Replies_Flag or (Num_Comments==0) :
            comment_guid = str(uuid.uuid1())
            comment_content = "NA"
            send_to_commentsDB (post_guid,comment_guid,comment_content)

        elif Down_Arrow_Flag:

            while Down_Arrow_Flag:         
                try:
    ##                driver.find_element_by_css_selector('.down-arrow').click()
                    click_load_button()
                except :
                    print ("no more load buttons")
                    Down_Arrow_Flag = 0            

            soup = bs.BeautifulSoup(driver.page_source, 'lxml')
    ##        The line below doesn't look necessary!!!!!
    ##        Down_Arrow_Flag = len(soup.find_all('div',class_='down-arrow'))    
            AllComments = soup.find_all('div',attrs={"id":"topic-reply"})
            
            for comment in AllComments:
                
                 comment_guid = str(uuid.uuid1())
                 comment_content = (comment.find('div',class_='content').text)

                 send_to_commentsDB (post_guid,comment_guid,comment_content)
                 
                 RepliesCounter = 0
                 RepliesList = []
                 AllReplies = comment.find_all('div',class_='subreply-content')
                 
                 for reply in AllReplies:

                     reply_guid = str(uuid.uuid1())
                     reply_content = (reply.text.lstrip())
    ##                 reply_no = str(comment_no)+"_"+str(RepliesCounter+1)               

                     send_to_repliesDB(comment_guid,reply_guid,reply_content)
                     
                     RepliesCounter +=1
                     
                 CommentsCounter += 1
                 
                 
        else:
                
            AllComments = soup.find_all('div',attrs={"id":"topic-reply"})
            
            for comment in AllComments:
                
                 comment_guid = str(uuid.uuid1())
                 comment_content = (comment.find('div',class_='content').text)

                 send_to_commentsDB (post_guid,comment_guid,comment_content)
                 
                 RepliesCounter = 0
                 RepliesList = []
                 AllReplies = comment.find_all('div',class_='subreply-content')
                 
                 for reply in AllReplies:

                     reply_guid = str(uuid.uuid1())
                     reply_content = (reply.text.lstrip())
    ##                 reply_no = str(comment_no)+"_"+str(RepliesCounter+1)               

                     send_to_repliesDB(comment_guid,reply_guid,reply_content)
                     
                     RepliesCounter +=1
                     
                 CommentsCounter += 1
##    except UnexpectedAlertPresentException:
##        self.handle_alert()
##        self.ScrapComments(post_guid,post_url,driver)

    except:
        self.driver.refresh()
        self.ScrapComments(post_guid,post_url,driver)          
